
lexons=_{SOI ~ lexon ~ lexon* ~ EOI }

lexon= { 
	lex ~
	lexver ~
	comments? ~
	terms ~
	contracts*
}

lex={_lex ~ "&"? ~ name ~  dot}

_lex={ ^"lex"|^"charta"}

lexver={(^"LEXON:" ~ version )?}

comments={ comment ~ comment* }

comment= _{ ^"comment:" ~ cmt }

cmt= @{(!NEWLINE ~ ANY )* ~ NEWLINE+}

statement= _ {
	clausecall|comment|definition|be|with|set|pay|receive|add|subtract|
	terminate|reveal|appoint|ifstmt|forstmt|certify|
	fix|_return|deem|
	increase|decrease|increased|decreased
}

terms={(^"terms:")? ~ stmts? ~ clauses?}

contracts={^"contracts" ~ ^"per" ~ name ~ ":" ~ stmts? ~ clauses?}

clauses={clause ~ clause* }

clause={^"clause:" ~ clausename ~ dot ~ stmts }

clausename={ "&"~symbol }

clausecall={filler? ~ clausename ~ callparams? }

callparams=_{ ^"for" ~ parameters }

//parameters={ symbol ~ ("," ~ symbol)* } // ambiguous

parameters={ symbol ~ ( ("," ~ symbol)* ~ ";")? }

stmts={(statement|may)  ~ ( separator? ~ (statement|may))* ~ dot?  }

separator=_{ and|seq }

seq={dot|iac}

iac=_{ ^"In any case, afterwards"}

and=_{ (","? ~ _and) | commasep }

commasep={","}

_and={ ^"and" ~ __and? }

__and={ ^"also" | ^"afterwards" | ^"thereby"| ^"then" }

definition={ varnameq ~ (^"is"|^"are") ~ filler? ~ _type}

be = {symbol ~ _be ~ _def ~ _as?}

_be = { ^"be"|^"is"|^"are" }

_def={^"certified"|^"appointed"|^"fixed"|deem|^"terminated"|^"notified"}

deem={_deem ~ symbol ~ status? }

_deem={^"deemed"|^"deem"}

fix={ (symbol~ &_fix)? ~ _fix ~ symbol ~ _as? }

_fix={ ^"fixes"|^"fix"|^"sets"|^"set"}

with={ ^"with" ~ symbol ~ ^"fixed" ~ _as}

_as={ ^"as" ~ expression }

an ={ (^"an"|^"a")}

set={ symbol ~ ^"sets" ~ an? ~ symbol ~ ( ^"and" ~ an? ~ symbol)*  }

appoint={ _appoint ~ ( asnew ~ symbol )? ~ symbol }

_appoint={(^"appoints"|^"appoint")}

asnew={^"as" ~ filler? ~ ^"new"}

certify={ (symbol ~ &_certify)? ~ _certify   }

_certify={( ^"certifies"|^"certify") ~ symbol ~ ( ^"and" ~ symbol)* }

pay={ pay_who? ~ _pay ~ pay_from? ~ expression ~ _into ~  pay_to }

pay_who={( symbol ~ &_pay)}

pay_from = {  (^"from" ~ symbol) }

pay_to = {symbol|_self}

_self={(("him"|"her") ~ "self") | "themselves"}

_pay=_{^"pays"|^"pay"}

_into={(^"to"|^"into")}

receive={_receive ~ expression}

_receive={^"receive" | ^"receives"}

may={ _when? ~ symbol ~ ^"may" ~ maystmts }

maystmts={statement  ~ ( separator? ~ (statement))* ~ dot?  }

_when={(^"at"  ~ ^"any" ~  ^"time")}

ifstmt={ ^"if" ~ condition ~ ^"then" ~ ontrue ~ ( ^"else" ~ onfalse )?}

ontrue={stmts}

onfalse={stmts}

condition={boolexression}

forstmt={^"for" ~ ^"all" ~ symbol ~ stmts  }

_return={^"return" ~ _return_and ~ (  ^"to" ~ symbol )? }

_return_and=_{expression ~ ( ^"and" ~ expression)* }

reveal={^"reveal" ~ reveal_list ~ (^"to" ~ symbol)? }

reveal_list={symbol ~ ((^"and"|",") ~ symbol )* }

terminate={ ^"terminate" ~ ( _thisctr|_allctrs) }

_thisctr ={ ^"this" ~ (^"contract"|symbol)}

_allctrs ={ ^"all" ~ ^"contracts"}

boolexression={(boolexpr ~ ( boolop ~  boolexpr)*)}

boolop={ ^"and"|^"or"}

boolexpr =_{ boolstmt| cmp |date |expr_is}

boolstmt={pay}

expr_is={(filler|^"this")? ~ sym ~ is ~ expression }

date={ symbol ~ is ~ timecmp }

is=_{_isnot|_is}

_isnot={^"is" ~ ^"not"}
_is   ={ ^"is"}

timecmp=_{now_or_past|now_or_future|past|future|now}

now_or_past  ={(now ~ ^"or" ~ past  ) | (past   ~ ^"or" ~ now)}

now_or_future={(now ~ ^"or" ~ future) | (future ~ ^"or" ~ now)}

future={^"future"}

past={^"past"}

now={^"now"}

cmp={expression ~ ^"is" ~  (geq|leq|ne|le|gt|eq) ~ expression}

geq={ ((^"greater" ~ ^"or" ~ ^"equal")|(^"equal" ~ ^"or" ~ ^"greater" )) ~ than? }

leq={ ((^"less"    ~ ^"or" ~ ^"equal")|(^"equal" ~ ^"or" ~ ^"less" ) )  ~ than? }

ne ={ ^"not" ~ ^"equal" ~ than? }

gt ={ ^"greater" ~ than? }

le ={ ^"less"    ~ than? }

eq ={ ^"equal"   ~ ^"to"? }

than={^"than"| ^"to"}

_type={ _thisctr | ^"person"|^"amount"|^"time"|^"key"|^"data"|^"text"|^"binary" | ^"asset"}

status={^"accepted"|^"denied"}

boolean={^"true"|^"false"}

expression  ={ term ~ ( plusminus ~ term)*}

plusminus = {plus |minus}

multdiv  ={ times | dividedby}

power    = {base~ _power ~ exponent}

add      = {^"add" ~ expression ~ ^"to" ~ symbol }

subtract = {^"subtract" ~ expression ~ ^"from" ~ symbol }

increase = { (^"increase"|^"increment") ~ symbol ~ ^"by" ~ expression}

decrease = { (^"decrease"|^"decrement") ~ symbol ~ ^"by" ~  expression}

increased = { symbol ~ _be ~ (^"increased"|^"incremented") ~ ^"by" ~ expression}

decreased = { symbol ~ _be ~ (^"decreased"|^"decremented") ~ ^"by" ~ expression}

base     = {(integer|symbol)}

exponent = {(integer|symbol)}

term     = { factor ~ ( multdiv ~ factor)* }

remainder= {filler? ~ _remainder ~ symbol}

_remainder=_{((^"remainder" ~ ^"of") | ^"remaining")}

sum={^"sum" ~ ^"of" ~ ^"all" ~ symbol }

time={filler? ~ ^"current" ~ ^"time" }

factor ={ 
	"(" ~ expression ~ ")" | 
	power|
	_type| 
	status | 
	remainder | 
	time| sum | 
	integer |
	given |
	symbol |
	clausecall }

plus     =_{ "+" | ^"plus"      }
minus    =_{ "-" | ^"minus"     }
times    =_{ "*" | "×"| ^"times"    }
dividedby=_{ "/" | "÷"| ^"divided by"}
_power   =_{ "**"| (^"to"~^"the"~^"power"~^"of")}

version={ integer ~ dot ~ integer ~ dot ~ integer  }

number=@{ "-"? ~ " "* ~ integer}

dot=_{"."}

varnameq={ filler? ~ quoteOpen ~ syms ~ quoteClose }

name=@{ filler? ~ syms}

syms={sym ~ ( " "+ ~ sym)*}

symbol={filler? ~ given? ~ sym}

given={^"given"}

sym=@{ ('a'..'z'|'A'..'Z'|'0'..'9'|"_"|"-")+}

integer={"-"? ~ ('0'..'9')+}

quoteOpen= _{"“" | "\"" | "‘"|"'"}

quoteClose= _{"”" | "\"" | "’"|"'"}

filler={ (^"their"| ^"the "|^"any" | ^"an" | ^"at" | ^"into" | ^"a ")}

wp= _ {" "}

sp=_{(" "|NEWLINE)*}

WHITESPACE = _{ "\r\n" | "\n" | " "  }

keyword={
	^"with"|
	^"is"|
	^"be"|
	^"are"|
	^"set"|
	^"pays"|
	^"pay"|
	^"add"|
	^"subtract"|
	^"terminate"|
	^"reveal"|
	^"may"|
	^"appoints"|
	^"appoint"|
	^"if"|
	^"for"|
	^"certifies"|
	^"certified"|
	^"certify"|
	^"fixed"|
	^"fixes"|
	^"fix"|
	^"increase"|
	^"increased"|
	^"increment"|
	^"incremented"|
	^"decrease"|
	^"decreased"|
	^"decrement"|
	^"decremented"|
	^"return"|
	^"deemed"|
	^"deems"|
	^"deem"
}
