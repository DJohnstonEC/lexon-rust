CHARTA COOP.
LEXON: 0.2.21
COMMENT: 21.a - coop charta snippet 1 

TERMS:

“Founder” is a person.
“Director” is a person.
“Applicant” is a person.
 
“Total Shares” is an amount.
“Application Fee” is an amount.
“Application Shares” is an amount.
“Founder’s Initial Number Of Shares” is an amount.

Membership is established for the Founder with the Founder’s Initial Number Of Shares. 

The Founder fixes the Application Fee, and also fixes the Application Shares.

The Founder is declared Director.

CLAUSE: Set Fee And Applicant Shares.
The Director may set the Application Fee and set the number of Application Shares.

CLAUSE: Membership Application.
The Director may, if an Applicant pays the Application Fee into escrow, then Membership is established for the Applicant with the Application Shares.

CONTRACTS PER MEMBERSHIP:
 
“Member” is a person.
“Owned Shares” is an amount.

The Member is appointed. The Owned Shares are appointed. The number of Total Shares is increased by the number of Application Shares.

CLAUSE: Leave.
A Member may receive the Payout, and afterwards Return All Shares. The Membership is terminated.

CLAUSE: Payout.
The “Payout” is defined as the amount in escrow divided by the number of Total Shares times the number of Owned Shares.

CLAUSE: Return All Shares.
The number of Total Shares are decreased by the number of Owned Shares.
Afterwards, Owned Shares are 0.
