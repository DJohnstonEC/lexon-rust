LEX Paid Escrow。
LEXON: 0.2.22
COMMENT: 3.f.j-4 - an escrow that is controlled by a third party for a fee。
COMMENT: going Kanji: 4) clause keyword。

「Payer」 is a person。
「Payee」 is a person。
「Arbiter」 is a person。
「Fee」 is an amount。

The Payer pays an Amount into escrow、
appoints the Payee、
appoints the Arbiter、
and also fixes the Fee。

条項: Pay Out。
The Arbiter may pay from escrow the Fee to themselves、
and afterwards pay the remainder of the escrow to the Payee。

条項: Pay Back。
The Arbiter may pay from escrow the Fee to themselves、
and afterwards return the remainder of the escrow to the Payer。
