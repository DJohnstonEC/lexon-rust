use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use lexon::*;
//use lexon::vm::LexonVM;

fn main() {
	let args: Vec<String> = std::env::args().collect();

	let unparsed_file = std::fs::read_to_string(&args[1]).expect("cannot read file");
  

  match ast::parse(unparsed_file){
    Ok(lexons)=>{     
      let vm=vm::LexonVM::new(lexons);
      
      // println!("{:#?}",vm);
      println!("\nResulting Ethereum Solidity code:\n");
      let sol=vm.solidity();
      println!("{}",sol);
      check_sol(sol);

      println!("\nResulting Aeternity Sophia code:\n");
      let sop=vm.sophia();

      println!("{}",sop);
      check_sop(sop);

      //println!("\nEnglish:\n");
      //println!("{}",vm.english());
      //println!("{}",vm.get_args());
      //println!("{}",vm.get_json());
            
    },
    Err(error)=>{
      println!("error: {:?}",error);
    }
  };

}

fn check_sol(sol: String){
  let path = Path::new("/tmp/tmpfile.sol");
  let display = path.display();

  let mut file = match File::create(&path) {
      Err(why) => panic!("couldn't create {}: {}",
                         display,
                         why.description()),
      Ok(file) => file,
  };

  match file.write_all(sol.as_bytes()) {
      Err(why) => {
          panic!("couldn't write to {}: {}", display,
                                             why.description())
      },
      Ok(_) => println!("Solidity code written to {}", display),
  }
}

fn check_sop(sop: String){
  let path = Path::new("/tmp/tmpfile.aes");
  let display = path.display();

  let mut file = match File::create(&path) {
      Err(why) => panic!("couldn't create {}: {}",
                         display,
                         why.description()),
      Ok(file) => file,
  };

  match file.write_all(sop.as_bytes()) {
      Err(why) => {
          panic!("couldn't write to {}: {}", display,
                                             why.description())
      },
      Ok(_) => println!("Sophia code written to {}", display),
  }
}
